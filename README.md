# Pizza Preferita backend

Build with Laravel Lumen.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## How to set up
### Requirements
- PHP >= 7.2
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension

### Build
- Run composer: 
`php composer install`
- Set up DB config: `.env` 
- Run DB demo data: `./artisan migrate:fresh --seed`
- Run tests: `./vendor/bin/phpunit`

## Live demo
https://still-fortress-13598.herokuapp.com/
