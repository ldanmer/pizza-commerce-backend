<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class Address extends ExtendedModel
{
    /**
     * The attributes that are mass assignable
     *
     * @var string[]
     */
    protected $fillable = ['city', 'address'];

    /**
     * The attributes excluded from the model's JSON form
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Gets order list
     *
     * @return HasMany
     */
    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    /**
     * Creates or gets Address model from request
     *
     * @param Request $request
     * @return Address|JsonResponse
     */
    public function createOrGetFromRequest(Request $request)
    {
        try {
            return Address::firstOrCreate([
                'city' => $request->input('address.city'),
                'address' => $request->input('address.address'),
            ]);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return response()->json('Cannot save Address model. Check the log for details', 500);
        }
    }
}
