<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class Customer extends ExtendedModel
{
    /**
     * The attributes that are mass assignable
     *
     * @var string[]
     */
    protected $fillable = ['name', 'email', 'phone'];

    /**
     * The attributes excluded from the model's JSON form
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Gets order list
     *
     * @return HasMany
     */
    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    /**
     * Creates or gets Customer model from request
     *
     * @param Request $request
     * @return Customer|JsonResponse
     */
    public function createOrGetFromRequest(Request $request)
    {
        try {
            return Customer::firstOrCreate([
                'name' => $request->input('customer.name'),
                'email' => $request->input('customer.email'),
                'phone' => $request->input('customer.phone'),
            ]);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return response()->json('Cannot save Customer model. Check the log for details', 500);
        }
    }
}


