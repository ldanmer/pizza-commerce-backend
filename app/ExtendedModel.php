<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ExtendedModel extends Model
{
    protected static function boot()
    {
        parent::boot();
        self::creating(function($model) {
            $model->id = Str::orderedUuid();
        });
    }

    public $incrementing = false;
}
