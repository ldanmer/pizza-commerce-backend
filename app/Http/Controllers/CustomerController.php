<?php

namespace App\Http\Controllers;

use App\Customer;

class CustomerController extends Controller
{
    public function read(string $email)
    {
        if($customer = Customer::where('email', $email)->first()) {
            return response()->json($customer);
        }
        return response()->json('Customer not found', 204);
    }
}
