<?php

namespace App\Http\Controllers;

use App\Option;

class OptionController extends Controller
{
    public function read(string $key)
    {
        return response()->json(Option::where('key', $key)->first());
    }
}
