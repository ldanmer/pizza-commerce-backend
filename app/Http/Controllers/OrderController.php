<?php

namespace App\Http\Controllers;

use App\Address;
use App\Customer;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class OrderController extends Controller
{
    public function read(string $email)
    {
        $customer = Customer::with('orders', 'orders.products')->where('email', $email)->first();
        return response()->json($customer->orders);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'sum'             => 'required|numeric|digits_between:1,8',
            'deliveryPrice'   => 'required|numeric|digits_between:1,6',
            'customer.name'   => 'required|string:max:255',
            'customer.email'  => 'required|email',
            'customer.phone'  => 'required',
            'address.city'    => 'required',
            'address.address' => 'required',
            'products'        => 'required'
        ]);

        $customer = (new Customer())->createOrGetFromRequest($request);
        $address = (new Address())->createOrGetFromRequest($request);

        if ($customer instanceof Customer && $address instanceof Address) {

            $order = new Order([
                'sum'           => $request->input('sum'),
                'deliveryPrice' => $request->input('deliveryPrice'),
                'customer_id'   => $customer->id,
                'address_id'    => $address->id
            ]);


            $orderList = [];

            if (is_iterable($request->input('products'))) {
                foreach ($request->input('products') as $product) {
                    $orderList[$product['id']] = ['amount' => $product['amount']];
                }
            }


            try {
                $order->save() && $order->products()->sync($orderList);
                return response()->json($order->customer, 201);
            } catch (\Exception $exception) {
                Log::error($exception->getMessage());
                return response()->json('Error saving order', 500);
            }
        }
        return response()->json('Internal server error', 500);
    }
}
