<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Order extends ExtendedModel
{
    protected $fillable = ['deliveryPrice', 'sum', 'status', 'customer_id', 'address_id'];
    /**
     * Gets customer
     *
     * @return BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    /**
     * Gets delivery address
     *
     * @return BelongsTo
     */
    public function address()
    {
        return $this->belongsTo('App\Address');
    }

    /**
     * Gets order`s composition
     *
     * @return BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Pizza')->withPivot('amount');
    }

    protected static function boot()
    {
        parent::boot();
        Order::creating(function($model) {
            $model->status = 'pending';
        });
    }
}
