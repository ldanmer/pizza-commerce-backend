<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Pizza extends ExtendedModel
{
    /**
     * The attributes that are mass assignable
     *
     * @var string[]
     */
    protected $fillable = ['name', 'image', 'base_price', 'description'];

    /**
     * The attributes excluded from the model's JSON form
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Gets order`s composition
     *
     * @return BelongsToMany
     */
    public function orders()
    {
        return $this->belongsToMany('App\Order');
    }
}
