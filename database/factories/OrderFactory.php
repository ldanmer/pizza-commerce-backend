<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'deliveryPrice' => $faker->randomNumber(4),
        'sum' => $faker->randomNumber(6),
        'status' => $faker->randomElement(['pending', 'approved', 'delivered', 'canceled']),
        'address_id' => factory('App\Address')->create()->id,
        'customer_id' => factory('App\Customer')->create()->id,
    ];
});
