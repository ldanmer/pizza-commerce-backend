<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Pizza;
use Faker\Generator as Faker;

$factory->define(Pizza::class, function (Faker $faker) {
    $random = $faker->randomNumber(1);
    return [
        'name' => $faker->word,
        'image' => 'images/pizza' . $random . '.png',
        'description' => $faker->realText(),
        'base_price' => $faker->randomNumber(3)
    ];
});
