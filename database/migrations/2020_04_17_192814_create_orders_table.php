<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->unsignedFloat('deliveryPrice', 6, 2);
            $table->unsignedFloat('sum', 8, 2);
            $table->enum('status', ['pending', 'approved', 'delivered', 'canceled']);
            $table->uuid('address_id');
            $table->uuid('customer_id');
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->foreign('customer_id')->references('id')->on('customers')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
