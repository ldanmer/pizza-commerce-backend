<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Set options for application
         *
         * @EUR - base currency
         * @USD - second currency to the base one
         * @delivery - base delivery cost for 1km
         */
        DB::table('options')->insert([
            ['key' => 'EUR', 'value' => '1'],
            ['key' => 'USD', 'value' => '1.09'],
            ['key' => 'delivery', 'value' => '5'],
        ]);
    }
}
