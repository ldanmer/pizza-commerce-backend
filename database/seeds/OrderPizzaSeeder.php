<?php

use App\Order;
use App\Pizza;
use Illuminate\Database\Seeder;

class OrderPizzaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pizzas = Pizza::all();
        Order::all()->each(function ($order) use ($pizzas) {
            $order->products()->attach(
                $pizzas->random(rand(1, 3))->pluck('id')->toArray(), ['amount' => random_int(1, 3)]
            );
        });
    }
}
