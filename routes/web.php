<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
    // Pizzas
    $router->group(['prefix' => 'pizzas'], function () use ($router) {
        $router->get('', 'PizzaController@index');
    });

    // Orders
    $router->group(['prefix' => 'orders'], function () use ($router) {
        $router->get('/{email}', 'OrderController@read');
        $router->post('/create', 'OrderController@store');
    });

    // Customers
    $router->group(['prefix' => 'customers'], function () use ($router) {
        $router->get('/{email}', 'CustomerController@read');
    });

    // Options
    $router->group(['prefix' => 'options'], function () use ($router) {
        $router->get('/{key}', 'OptionController@read');
    });
});
