<?php

use App\Customer;

class CustomerTest extends TestCase
{
    const EMAIL = 'mail@example.ru';
    public function setUp(): void
    {
        parent::setUp();
        $faker = Faker\Factory::create();
        $email = self::EMAIL;
        Customer::create([
            'name'  => $faker->name,
            'phone' => $faker->phoneNumber,
            'email' => $email
        ]);
    }

    /**
     * @test
     * api/customers/{email} [GET]
     */
    public function shouldReturnCustomerByEmail(): void
    {
        $this->get('api/customers/' . self::EMAIL, [])
            ->seeStatusCode(200)
            ->seeJsonStructure([
            'name', 'email', 'phone'
        ]);
    }

    public function tearDown(): void
    {
        Customer::where('email', self::EMAIL)->delete();
        parent::tearDown();
    }
}
