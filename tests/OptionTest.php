<?php

use App\Option;

class OptionTest extends TestCase
{
    const KEY = 'RUB';
    const VALUE = '81.51';
    public function setUp(): void
    {
        parent::setUp();
        Option::create([
            'key'   => self::KEY,
            'value' => self::VALUE
        ]);
    }

    /**
     * @test
     * api/options/{key} [GET]
     */
    public function shouldReturnValueByKey(): void
    {
        $this->get('api/options/' . self::KEY, [])
            ->seeStatusCode(200)
            ->seeJson([
               'key' => self::KEY,
               'value' => self::VALUE
            ]);
    }

    public function tearDown(): void
    {
        Option::where('key', self::KEY)->delete();
        parent::tearDown();
    }
}
