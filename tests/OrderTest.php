<?php


use App\Customer;
use App\Order;
use App\Pizza;

class OrderTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     * api/orders/{email} [GET]
     */
    public function testShouldReturnOrdersByCustomer(): void
    {
        $faker = Faker\Factory::create();
        $email = 'mail@example.ru';
        $customer_id = Customer::create([
            'name'  => $faker->name,
            'phone' => $faker->phoneNumber,
            'email' => $email
        ])->id;

        Order::create([
            'deliveryPrice' => $faker->randomNumber(4),
            'sum'           => $faker->randomNumber(6),
            'status'        => $faker->randomElement(['pending', 'approved', 'delivered', 'canceled']),
            'address_id'    => factory('App\Address')->create()->id,
            'customer_id'   => $customer_id,
        ]);

        $this->get('api/orders/' . $email, [])->seeStatusCode(200)->seeJsonStructure([
            '*' => [
                'deliveryPrice', 'sum', 'status'
            ]
        ]);
        Customer::where('email', $email)->with('orders')->delete();
    }

    /**
     * @test
     * api/orders/create [POST]
     */
    public function testShouldOrderCreate(): void
    {
        $faker = Faker\Factory::create();
        $currentDate = Carbon\Carbon::now();
        $pizza = Pizza::all()->random();

        $postData = [
            'deliveryPrice' => $faker->randomNumber(4),
            'sum'           => $faker->randomNumber(6),
            'status'        => $faker->randomElement(['pending', 'approved', 'delivered', 'canceled']),
            'products'      => [[
                                    'id'     => $pizza->id,
                                    'amount' => $faker->randomNumber(1)
                                ]],
            'customer'      => [
                'name'  => $faker->name,
                'phone' => $faker->phoneNumber,
                'email' => $faker->email
            ],
            'address'       => [
                'city'    => $faker->city,
                'address' => $faker->address,
            ],
            'created_at' => $currentDate

        ];
        $this->post('api/orders/create', $postData)
            ->seeStatusCode(201)
            ->seeInDatabase('orders', ['created_at' => $currentDate]);

        Order::where('created_at', $currentDate)->with('customer', 'address')->delete();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }
}
