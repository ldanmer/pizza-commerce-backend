<?php

class PizzaTest extends TestCase
{
    /**
     * @test
     * api/pizzas [GET]
     */
    public function testShouldReturnAllProducts()
    {
        $this->get('api/pizzas', [])->seeStatusCode(200)
            ->seeJsonStructure([
                '*' => [
                    'name', 'image', 'description', 'base_price'
                ]
            ]);
    }
}
